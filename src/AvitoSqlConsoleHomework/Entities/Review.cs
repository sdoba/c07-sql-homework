﻿using System;

namespace AvitoSqlConsoleHomework.Entities
{
    public class Review
    {
        public long? Id { get; set; }
        
        public long? ItemId { get; set; }
        
        public Item Item { get; set; }
        
        public int? AuthorId { get; set; }
        
        public User Author { get; set; }
        
        public string Description { get; set; }
        
        public short? Rating { get; set; }
        
        public bool? DidPurchase { get; set; }
        
        public DateTime? DateCreated { get; set; }
    }
}