﻿using System;

namespace AvitoSqlConsoleHomework.Entities
{
    public class Item
    {
        public long? Id { get; set; }
        
        public int? OwnerId { get; set; }
        
        public User Owner { get; set; }
        
        public string Name { get; set; }
        
        public float? Price { get; set; }
        
        public string Description { get; set; }
        
        public DateTime? DateCreated { get; set; }
    }
}