﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoSqlConsoleHomework.Helpers
{
    public class SqlTableMapper
    {
        public readonly string Name;
        public readonly string Alias;
        public readonly string NameWithAlias;
        
        public readonly string KeyPropertyName;
        public readonly string Key;
        public readonly string KeyInTable;

        private readonly Dictionary<string, string> _columnMapping;

        public SqlTableMapper(string tableName, string tableAlias, Dictionary<string, string> columnMapping,
            string keyPropertyName)
        {
            Name = tableName;
            Alias = tableAlias;
            NameWithAlias = $"{Name} AS {Alias}";
            
            _columnMapping = columnMapping;
            
            KeyPropertyName = keyPropertyName;
            Key = Column(keyPropertyName);
            KeyInTable = ColumnInTable(keyPropertyName);
        }

        public string NameWithAliasOnEqualKeysWith(SqlTableMapper leftTable)
        {
            return $"{NameWithAlias} ON {leftTable.KeyInTable} = {KeyInTable}";
        }

        public string Column(string propertyName)
        {
            if (_columnMapping.TryGetValue(propertyName, out var columnName))
                return columnName;
            
            throw new ArgumentException($"Dictionary has no mapping for \"{propertyName}\" property");
        }
        
        public string Columns(string propertyName, params string[] otherPropertyNames)
        {
            return JoinMappings(Column, propertyName, otherPropertyNames);
        }
        
        public string ColumnInTable(string propertyName)
        {
            return $"{Alias}.{Column(propertyName)}";
        }
        
        public string ColumnsInTable(string propertyName, params string[] otherPropertyNames)
        {
            return JoinMappings(ColumnInTable, propertyName, otherPropertyNames);
        }
        
        public string ColumnAsMapped(string propertyName)
        {
            return $"{ColumnInTable(propertyName)} AS {propertyName}";
        }
        
        public string ColumnsAsMapped(string propertyName, params string[] otherPropertyNames)
        {
            return JoinMappings(ColumnAsMapped, propertyName, otherPropertyNames);
        }
        
        public string ColumnSetToParameter(string propertyName)
        {
            return $"{Column(propertyName)} = @{propertyName}";
        }

        public string ColumnsSetToParameters(string propertyName, params string[] otherPropertyNames)
        {
            return JoinMappings(ColumnSetToParameter, propertyName, otherPropertyNames);
        }
        
        private string JoinMappings(Func<string, string> mappingFunction, string propertyName,
            params string[] otherPropertyNames)
        {
            var result = new StringBuilder(mappingFunction(propertyName));
            
            for (var i = 0; i < otherPropertyNames.Length; i++)
                result.Append($", {mappingFunction(propertyName)}");

            return result.ToString();
        }
    }
}