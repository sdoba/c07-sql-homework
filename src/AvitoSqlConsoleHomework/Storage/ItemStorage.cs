﻿using System;
using System.Collections.Generic;
using System.Linq;
using AvitoSqlConsoleHomework.Entities;
using AvitoSqlConsoleHomework.Helpers;
using Dapper;
using Npgsql;

namespace AvitoSqlConsoleHomework.Storage
{
    public class ItemStorage
    {
        public static readonly SqlTableMapper Table;

        public static readonly string TableColumnsAllAsMapped;
        public static readonly string TableColumnsWithoutKeySetToParameters;
        public static readonly string TableColumnsWithoutKey;
        public static readonly string TableParametersWithoutKey;

        static ItemStorage()
        {
            Table = new SqlTableMapper(
                "items", "itm", new Dictionary<string, string>
                {
                    { nameof(Item.Id), "id" },
                    { nameof(Item.OwnerId), "owner_id" },
                    { nameof(Item.Name), "name" },
                    { nameof(Item.Price), "price" },
                    { nameof(Item.Description), "description" },
                    { nameof(Item.DateCreated), "date_created" }
                }, nameof(Item.Id));
            
            TableColumnsAllAsMapped = Table.ColumnsAsMapped(nameof(Item.Id), nameof(Item.OwnerId),
                nameof(Item.Name), nameof(Item.Price), nameof(Item.Description), nameof(Item.DateCreated));
            
            TableColumnsWithoutKeySetToParameters = Table.ColumnsSetToParameters(nameof(Item.OwnerId),
                nameof(Item.Name), nameof(Item.Price), nameof(Item.Description),
                nameof(Item.DateCreated));
            
            TableColumnsWithoutKey = Table.Columns(nameof(Item.OwnerId), nameof(Item.Name),
                nameof(Item.Price), nameof(Item.Description), nameof(Item.DateCreated));
            
            TableParametersWithoutKey = $@"@{nameof(Item.OwnerId)}, @{nameof(Item.Name)}, @{nameof(Item.Price)},
                @{nameof(Item.Description)}, @{nameof(Item.DateCreated)}";
        }

        private bool RequiredPropertiesNotEmpty(Item item, out string errorMessage)
        {
            var missingProperties = new List<string>();
            
            if (item.OwnerId == null)
                missingProperties.Add(nameof(item.OwnerId));
            if (string.IsNullOrEmpty(item.Name))
                missingProperties.Add(nameof(item.Name));
            if (item.DateCreated == null)
                missingProperties.Add(nameof(item.DateCreated));

            if (missingProperties.Any())
            {
                errorMessage = "The following required properties must have value: " + string.Join(", ", missingProperties);
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }
        
        private readonly string _connectionString;

        public ItemStorage(string connectionString)
        {
            _connectionString = connectionString;
        }
        
        public Item Get(long? id)
        {
            if (id == null)
                throw new ArgumentException("Item Id can't be null");
            
            var sql = $"SELECT {TableColumnsAllAsMapped} FROM {Table.NameWithAlias} WHERE {Table.KeyInTable} = {id}";
            using var connection = new NpgsqlConnection(_connectionString);
            return connection.QuerySingleOrDefault<Item>(sql);
        }

        public List<Item> GetAll()
        {
            var sql = $"SELECT {TableColumnsAllAsMapped} FROM {Table.NameWithAlias} ORDER BY {Table.KeyInTable} ASC";
            using var connection = new NpgsqlConnection(_connectionString);
            return connection.Query<Item>(sql).ToList();
        }
        
        public long? Create(Item item)
        {
            if (item.Id != null)
                throw new ArgumentException($"Item must not have Id before creating");
            
            if (!RequiredPropertiesNotEmpty(item, out var errorMessage))
                throw new ArgumentException(errorMessage);

            var sql = $@"INSERT INTO {Table.Name}({TableColumnsWithoutKey}) VALUES ({TableParametersWithoutKey}) 
                RETURNING {Table.Key}";
            var parameters = new
            {
                item.OwnerId,
                item.Name,
                item.Price,
                item.Description,
                item.DateCreated
            };

            using var connection = new NpgsqlConnection(_connectionString);
            item.Id = connection.ExecuteScalar<long?>(sql, parameters);
            return item.Id;
        }
        
        public void Update(Item item)
        {
            if (item.Id == null)
                throw new ArgumentException($"Item must have Id before updating");
            
            if (!RequiredPropertiesNotEmpty(item, out var errorMessage))
                throw new ArgumentException(errorMessage);

            if (Get(item.Id) == null)
                throw new InvalidOperationException($"Item with Id={item.Id} doesn't exist in the DB");
            
            var sql = $@"UPDATE {Table.Name} SET {TableColumnsWithoutKeySetToParameters} 
                WHERE {Table.Key} = @{nameof(Item.Id)}";
            var parameters = new
            {
                item.Id,
                item.OwnerId,
                item.Name,
                item.Price,
                item.Description,
                item.DateCreated
            };

            using var connection = new NpgsqlConnection(_connectionString);
            connection.Execute(sql, parameters);
        }
        
        public void Delete(long? id)
        {
            if (id == null)
                throw new ArgumentException("Item Id can't be null");
            
            if (Get(id) == null)
                throw new InvalidOperationException($"Item with Id={id} doesn't exist in the DB");
            
            var sql = $@"DELETE FROM {Table.Name} WHERE {Table.Key} = {id}";

            using var connection = new NpgsqlConnection(_connectionString);
            connection.Execute(sql);
        }
    }
}