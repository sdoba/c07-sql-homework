CREATE TABLE public.users
(
    id serial NOT NULL,
    name character varying(100) NOT NULL,
    email character varying(254) NOT NULL,
    phone character varying(20),
    PRIMARY KEY (id),
    UNIQUE (email)
);

CREATE TABLE public.items
(
    id bigserial NOT NULL,
    owner_id integer NOT NULL,
    name character varying(100) NOT NULL,
    price real,
    description text,
    date_created timestamp without time zone NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (owner_id)
        REFERENCES public.users (id)
        ON DELETE CASCADE
);

CREATE TABLE public.reviews
(
    id bigserial NOT NULL,
    item_id bigint NOT NULL,
    author_id integer NOT NULL,
    description text,
    rating smallint NOT NULL,
    did_purchase boolean NOT NULL,
    date_created timestamp without time zone NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (item_id)
        REFERENCES public.items (id)
        ON DELETE CASCADE,
    FOREIGN KEY (author_id)
        REFERENCES public.users (id)
        ON DELETE CASCADE
);
