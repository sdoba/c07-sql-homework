INSERT INTO public.users(
	name, email, phone) VALUES
	('Александр Полянский', 'alexander@otus.homework.com', '+79209139999'),
	('Лёха', 'leha@gmail.qa', '+7(999)852-41-14'),
	('Invisible Man', 'invisible@man.test', NULL),
	('Маша', 'masha@masha.ru', '8 900 011 23 77'),
	('Какой-то пользователь', 'не@скажу.рф', NULL);

INSERT INTO public.items(
	owner_id, name, price, description, date_created) VALUES
	(1, 'Телефон Xiaomi Mi 8 б/у цена договорная', NULL, 'Подвезу к метро, любые проверки', '2022-11-11'),
	(2, 'ВИДЯХА RTX 4090 НОВАЯ', 500000, NULL, '2022-12-09 01:59:59'),
	(1, 'Аквариум с тумбой самовывоз', 2000, 'Упаковки нет, но могу помочь с погрузкой', '2022-09-23 18:12:01'),
	(4, 'детский самокат', 500, 'подсветка на колёсах работает', '2022-12-10 11:34:56.955032'),
	(4, 'odejda detskaya otdam darom', NULL, NULL, current_timestamp);

INSERT INTO public.reviews(
	item_id, author_id, description, rating, did_purchase, date_created) VALUES
	(4, 1, 'Всё отлично!', 5, true, current_timestamp),
	(2, 1, 'Цена запредельная, торга нет!', 1, false, '2022-12-11 21:56:34.329550'),
	(2, 4, 'не берёт трубку...', 1, false, '2022-12-12 12:18:10'),
	(2, 5, NULL, 5, true, '2022-12-10 23:01:59'),
	(1, 5, NULL, 4, true, '2022-11-12');
